using System;
using UnityEngine;
using UnityEngine.AddressableAssets;
using UnityEngine.UI;

public class AddressableLoader : MonoBehaviour
{
    public string serverVersion = "0.1.0";
    public string url = "https://funky-subprograms.000webhostapp.com/addressable-assetbundle-comparison/addressables";
    public bool isClearCacheBeforeRun;

    public AssetReference nextSceneToLoad;


    private Slider downloadProgressSlider;
    private void Awake()
    {
        downloadProgressSlider = FindObjectOfType<Slider>();
    }

    private void SetDownloadProgress(float value)
    {
        downloadProgressSlider.value = value;
    }

    private async void Start()
    {
#if UNITY_EDITOR
        if (isClearCacheBeforeRun)
            Caching.ClearCache();
#endif

        Debug.Log($"Addressable Initialize status: { await ResourceManager.InitializeAddressables() }");
        Debug.Log($"Add catalog version status: { await ResourceManager.AddCatalogFromURL($"{url}/catalog_{serverVersion}.json") }");
        Debug.Log($"Get download size: { (float)await ResourceManager.GetDownloadSize() / 1024 / 1024 } MB");
        Debug.Log($"Download status: { await ResourceManager.DownloadDependency() }");

        await ResourceManager.LoadScene(nextSceneToLoad, UnityEngine.SceneManagement.LoadSceneMode.Single);
    }

    private void OnEnable()
    {
        ResourceManager.DownloadDependencyProgress += SetDownloadProgress;
    }

    private void OnDisable()
    {
        ResourceManager.DownloadDependencyProgress -= SetDownloadProgress;
    }

}
