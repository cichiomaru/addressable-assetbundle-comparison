using System;
using System.Collections;
using System.Collections.Generic;
using System.Threading.Tasks;
using UnityEngine;
using UnityEngine.AddressableAssets;
using UnityEngine.AddressableAssets.ResourceLocators;
using UnityEngine.Networking;
using UnityEngine.ResourceManagement.AsyncOperations;
using UnityEngine.ResourceManagement.Exceptions;
using UnityEngine.ResourceManagement.ResourceLocations;
using UnityEngine.ResourceManagement.ResourceProviders;
using UnityEngine.SceneManagement;

public static class ResourceManager
{
    //addressables ops
    private static Dictionary<string, AsyncOperationHandle> instantiatedUniqueObjects = new Dictionary<string, AsyncOperationHandle>();
    private static Dictionary<string, AsyncOperationHandle<SceneInstance>> loadedScenes = new Dictionary<string, AsyncOperationHandle<SceneInstance>> ();

    //addressables initialization
    private static List<string> updatableCatalog = new();
    private static List<IResourceLocator> updatedResourceLocator = new();
    public static Dictionary<string, IResourceLocation> primaryKeys = new();

    //addressables initialization action
    public static event Action<float> DownloadDependencyProgress;
    public static event Action DownloadDependencyFailed;
    public static event Action DownloadDependencyStarting;
    public static event Action DownloadDependencyCompleted;


    public static event Action LoadSceneStarted;
    public static event Action<float> LoadSceneProgress;
    public static event Action LoadSceneFinished;

    public static async Task<bool> InitializeAddressables()
    {
        var resourceLocator = await Addressables.InitializeAsync(true).Task;

        return true;
    }

    private static void Release(AsyncOperationHandle handle)
    {
        Addressables.Release(handle);
    }

    public static async Task<AsyncOperationStatus> AddCatalogFromURL(string path)
    {
        AsyncOperationStatus status = AsyncOperationStatus.None;

        try
        {
            var loadCatalogHandle = Addressables.LoadContentCatalogAsync(path, false);
            await loadCatalogHandle.Task;
            
            if (loadCatalogHandle.Status == AsyncOperationStatus.Succeeded)
            {
                Debug.Log($"catalog added successfully");
            }
            else
            {
                Debug.Log("Failed to add load content catalog from path.");
            }

            status = loadCatalogHandle.Status;
            Release(loadCatalogHandle);
        }
        catch (RemoteProviderException rpe)
        {
            Debug.Log($"Invalid version. Catalog not found!");
        }

        return status;
    }

    private static IEnumerator GetPath()
    {
        string path = "https://funky-subprograms.000webhostapp.com/virtual-booth/get-addressable-version.php";

        UnityWebRequest request = UnityWebRequest.Get(path);
        yield return request.SendWebRequest();
    }

    public static async Task<bool> CatalogCheck()
    {
        var catalogCheckHandle = Addressables.CheckForCatalogUpdates(false);
        await catalogCheckHandle.Task;

        if (catalogCheckHandle.Status != AsyncOperationStatus.Succeeded)
        {
            return false;
        }

        updatableCatalog.AddRange(catalogCheckHandle.Result);
        Release(catalogCheckHandle);
        return true;
    }

    public static async Task<bool> CatalogUpdate()
    {
        if (updatableCatalog.Count == 0)
        {
            return true;
        }

        var updateHandle = Addressables.UpdateCatalogs(updatableCatalog, false);
        await updateHandle.Task;

        if (updateHandle.Status != AsyncOperationStatus.Succeeded)
        {
            return false;
        }

        updatedResourceLocator.AddRange(updateHandle.Result);
        Release(updateHandle);
        return true;
    }

    private static void RetrievePrimaryKey()
    {
        List<string> keys = new List<string>();

        foreach (var locator in Addressables.ResourceLocators)
        {
            foreach (var key in locator.Keys)
            {
                IList<IResourceLocation> locations;
                locator.Locate(key, null, out locations);

                if (locations == null)
                {
                    continue;
                }

                foreach (var location in locations)
                {
                    string primary = location.PrimaryKey;

                    if (primaryKeys.ContainsKey(primary))
                    {
                        break;
                    }

                    primaryKeys.Add(primary, location);
                    //keys.Add(primary);
                }
            }
        }
    }

    public static async Task<long> GetDownloadSize()
    {
        RetrievePrimaryKey();

        var downloadSizeHandle = Addressables.GetDownloadSizeAsync(primaryKeys.Keys);
        await downloadSizeHandle.Task;

        if (downloadSizeHandle.Status != AsyncOperationStatus.Succeeded)
        {
            return -1;
        }

        long sizeInBytes = downloadSizeHandle.Result;
        Release(downloadSizeHandle);

        return sizeInBytes;
    }

    public static async Task<AsyncOperationStatus> DownloadDependency()
    {
        DownloadDependencyStarting?.Invoke();

        var downloadHandle = Addressables.DownloadDependenciesAsync(primaryKeys.Keys, Addressables.MergeMode.Union, false);
        //var downloadHandle = Addressables.DownloadDependenciesAsync(primaryKeys.Values, Addressables.MergeMode.Union, false);

        while (!downloadHandle.IsDone)
        {
            DownloadDependencyProgress?.Invoke(downloadHandle.GetDownloadStatus().Percent);
            await Task.Yield();
        }

        if (downloadHandle.Status != AsyncOperationStatus.Succeeded)
        {
            DownloadDependencyFailed?.Invoke();
            Debug.Log($"Download failed");
            return AsyncOperationStatus.Failed;
        }

        DownloadDependencyCompleted?.Invoke();
        Release(downloadHandle);

        return AsyncOperationStatus.Succeeded;
    }

    public static async Task<Sprite> LoadImage(AssetReference reference)
    {
        var sprite = await Addressables.LoadAssetAsync<Sprite>(reference).Task;

        return sprite;
    }

    public static async Task<GameObject> InstantiateObject(string path)
    {
        var objectHandle = Addressables.InstantiateAsync(path);
        await objectHandle.Task;

        if (objectHandle.Status == AsyncOperationStatus.Succeeded)
        {
            instantiatedUniqueObjects.Add(path, objectHandle);
            return objectHandle.Result;
        }
        else
        {
            Debug.Log($"Fail to instantiate object: {path}");
            return null;
        }
    }
    
    public static async Task InstantiateObject(AssetReferenceT<GameObject> barbarian)
    {
        //var objectHandle = Addressables.InstantiateAsync(barbarian);
        var objectHandle = barbarian.InstantiateAsync();
        await objectHandle.Task;

        if (objectHandle.Status == AsyncOperationStatus.Succeeded)
        {
            instantiatedUniqueObjects.Add(barbarian.AssetGUID, objectHandle);
        }
        else
        {
            Debug.Log($"Fail to instantiate object: {barbarian.AssetGUID}");
        }
    }

    private static async Task InternalLoadScene(AsyncOperationHandle<SceneInstance> loadSceneHandle)
    {
        float loadSceneProgress = 0f;

        while (!loadSceneHandle.IsDone)
        {
            loadSceneProgress = loadSceneHandle.GetDownloadStatus().Percent;
            LoadSceneProgress?.Invoke(loadSceneProgress/* * 0.7f + activateSceneProgress * 0.3f*/);
            await Task.Delay(100);
        }

        if (loadSceneHandle.Status == AsyncOperationStatus.Succeeded)
        {
            Debug.Log($"Scene loaded succeed : {loadSceneHandle.Result.Scene.name}");
        }
        else if (loadSceneHandle.Status == AsyncOperationStatus.Failed)
        {
            Debug.Log($"Scene loaded failed : {loadSceneHandle.Result.Scene.name}");
        }

        loadedScenes.Add(loadSceneHandle.Result.Scene.path, loadSceneHandle);

        LoadSceneFinished?.Invoke();
    }

    public static async Task LoadScene(AssetReference reference, LoadSceneMode mode)
    {
        LoadSceneStarted?.Invoke();

        var loadSceneHandle = reference.LoadSceneAsync(mode, true);

        await InternalLoadScene(loadSceneHandle);
    }

    public static async Task LoadScene(string path, LoadSceneMode mode)
    {
        LoadSceneStarted?.Invoke();

        var loadSceneHandle = Addressables.LoadSceneAsync(path, mode, true);

        await InternalLoadScene(loadSceneHandle);
    }

    public static async Task UnloadScene(AssetReference reference)
    {
        var unloadSceneHandle = await reference.UnLoadScene().Task;
        Debug.Log(unloadSceneHandle.Scene.path);
        loadedScenes.Remove(unloadSceneHandle.Scene.path);
    }

    public static async Task UnloadScene(string path)
    {
        var unloadSceneHandle = Addressables.UnloadSceneAsync(loadedScenes[path], true);
        await unloadSceneHandle.Task;

        loadedScenes.Remove(path);
    }

}