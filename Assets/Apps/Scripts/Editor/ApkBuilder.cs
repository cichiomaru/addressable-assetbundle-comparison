using Sirenix.OdinInspector;
using Sirenix.OdinInspector.Editor;
using Sirenix.Serialization;
using System;
using System.IO;
using System.Threading.Tasks;
using UnityEditor;
using UnityEditor.AddressableAssets;
using UnityEditor.AddressableAssets.Build;
using UnityEditor.AddressableAssets.Settings;
using UnityEditor.Build.Reporting;
using UnityEngine;
using UnityEngine.SceneManagement;

namespace VirtualBooth.Utility
{
    public enum VersionUpdateType
    {
        MAJOR, MINOR, PATCH
    }

    public class ApkBuilder : OdinEditorWindow
    {
        #region Addressables
        [OdinSerialize, BoxGroup("Addressables", CenterLabel = true)] private VersionUpdateType addressableUpdateType;
        [OdinSerialize, BoxGroup("Addressables")] private bool isUpdateAddressableVersion;
        [OdinSerialize, BoxGroup("Addressables")] private bool isOverrideAddressableVersion;
        [OdinSerialize, BoxGroup("Addressables")] private string contentStateBuildPath = "Content State/Build_";
        [ShowIf("isOverrideAddressableVersion"), BoxGroup("Addressables"), OdinSerialize] private string addressableVersion;


        [Button, BoxGroup("Addressables")]
        public void BuildContentOnly()
        {
            // catalog version update
            if (isOverrideAddressableVersion)
            {
                OverrideAddressableVersion();
            }
            else if (isUpdateAddressableVersion)
            {
                UpdateAddressableVersion();
            }

            // content builder update
            AddressableAssetSettingsDefaultObject.Settings.ContentStateBuildPath = $"{contentStateBuildPath}{AddressableAssetSettingsDefaultObject.Settings.OverridePlayerVersion}";

            AddressablesPlayerBuildResult result;
            AddressableAssetSettings.BuildPlayerContent(out result);
        }

        [Button, BoxGroup("Addressables")]
        public static void ResetAddressable()
        {
            AddressableAssetSettings.CleanPlayerContent();
            AddressableAssetSettingsDefaultObject.Settings.OverridePlayerVersion = "0.0.0";
        }

        [Button, BoxGroup("Addressables")]
        public void OpenAddressableGroup()
        {
            EditorApplication.ExecuteMenuItem("Window/Asset Management/Addressables/Groups");
        }

        public void UpdateAddressableVersion()
        {
            string newVersion = GetNextVersion(addressableUpdateType, AddressableAssetSettingsDefaultObject.Settings.PlayerBuildVersion);

            AddressableAssetSettingsDefaultObject.Settings.OverridePlayerVersion = newVersion;
        }

        public void CheckUpdateRestriction()
        {
            ///https://docs.unity.cn/Packages/com.unity.addressables@1.18/manual/BuildPlayerContent.html
        }

        public void OverrideAddressableVersion()
        {
            AddressableAssetSettingsDefaultObject.Settings.OverridePlayerVersion = addressableVersion;
        }
        #endregion

        #region apk
        [OdinSerialize, BoxGroup("Apk", CenterLabel = true)] private VersionUpdateType apkUpdateType;
        [OdinSerialize, ReadOnly, BoxGroup("Apk")] private string apkBuildPath = System.IO.Directory.GetCurrentDirectory() + "/Build/Android/";
        [OdinSerialize, ReadOnly, BoxGroup("Apk")] private string apkName = "Virtual Booth";
        [OdinSerialize, BoxGroup("Apk")] private bool isBuildAppBundle;
        [OdinSerialize, BoxGroup("Apk")] private bool isOverrideApkVersion;
        [ShowIf("isOverrideApkVersion"), BoxGroup("Apk"), OdinSerialize] private string apkVersion;

        [Button, BoxGroup("Apk")]
        public void BuildApk()
        {
            //string[] defaultScene = new string[] { "Assets/Apps/Scenes/Asset Verification.unity" };

            var defaultScene = GetAllScene();

            UpdateApkVersion();

            string filename = $"{apkName}_v_{PlayerSettings.bundleVersion}";
            BuildReport report;

            if (isBuildAppBundle)
            {
                report = BuildPipeline.BuildPlayer(defaultScene, $"{apkBuildPath}{filename}.aab", BuildTarget.Android, BuildOptions.None);

                Debug.Log($"Build aab success in {report.summary.totalTime} seconds and {FileSize.ByteToMB((long)report.summary.totalSize)}MB");
            }

            EditorUserBuildSettings.buildAppBundle = false;
            report = BuildPipeline.BuildPlayer(defaultScene, $"{apkBuildPath}{filename}.apk", BuildTarget.Android, BuildOptions.None);

            Debug.Log($"Build apk success in {report.summary.totalTime} seconds and {FileSize.ByteToMB((long)report.summary.totalSize)}MB");
        }

        private void UpdateApkVersion()
        {
            if (isOverrideApkVersion)
            {
                PlayerSettings.bundleVersion = apkVersion;
            }
            else
            {
                string version = GetNextVersion(apkUpdateType, PlayerSettings.bundleVersion);
                PlayerSettings.bundleVersion = version;
            }
            Debug.Log($"Apk version: {PlayerSettings.bundleVersion}");
        }

        #endregion


        public string[] GetAllScene()
        {
            string[] defaultScene = new string[SceneManager.sceneCountInBuildSettings];

            for (int i = 0; i < defaultScene.Length; i++)
            {
                defaultScene[i] = SceneUtility.GetScenePathByBuildIndex(i);
                Debug.Log(defaultScene[i]);
            }
            return defaultScene;
        }

        public string GetNextVersion(VersionUpdateType type, string _version)
        {
            string[] version = _version.Split(".");

            int major = int.Parse(version[0]);
            int minor = int.Parse(version[1]);
            int patch = int.Parse(version[2]);

            switch (type)
            {
                case VersionUpdateType.MAJOR:
                    major++;
                    break;
                case VersionUpdateType.MINOR:
                    minor++;
                    break;
                case VersionUpdateType.PATCH:
                    patch++;
                    break;
            }

            return $"{major}.{minor}.{patch}";
        }

        [MenuItem("Apk Builder/Open Window")]
        private static void OpenWindow()
        {
            GetWindow<ApkBuilder>().Show();
        }


        [Button]
        public void BuildAssetBundle()
        {
            string assetBundleDirectory = "Assets/StreamingAssets";
            if (!Directory.Exists(Application.streamingAssetsPath))
            {
                Directory.CreateDirectory(assetBundleDirectory);
            }
            BuildPipeline.BuildAssetBundles(assetBundleDirectory, BuildAssetBundleOptions.None, EditorUserBuildSettings.activeBuildTarget);
        }
    }

    internal class FileSize
    {
        internal static string ByteToMB(long totalSize)
        {
            return $"{(float)totalSize/1024/1024}";
        }
    }
}